var express = require("express");
var bodyparser = require("body-parser");
var path = require("path");
var exphbs = require("express-handlebars");
var mysql = require("mysql"); 
var session = require('express-session');
var app=express();
app.set('views',path.join(__dirname,'view'));
app.set('view engine','hbs');
app.use(bodyparser.json());      
app.use(bodyparser.urlencoded({ extended : true}));
app.engine('hbs',exphbs({ extname:'hbs',defaultLayout:'mainLayout',layoutsDir:__dirname +'/view/default_layout/' }));
app.use(express.static(path.join(__dirname,'/public')));
app.use(session({secret: 'ssshhhhh',saveUninitialized: true,resave: true}));
var sess;
var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'test'
});
/*
conn.connect(function(err){
    if (err) throw err;
    console.log("Database connected");
});*/

app.get('/',function(req,res){

  /*sess = req.session;
  if(sess.uname != null) 
  {  

    str=sess.uname;
    res.render("home",{str});
  }
  else
  {
    res.render("index");    
  }*/
  res.render("home");     
});
/*
app.get('/reg_1',function(req,res){

    res.render("register");
});

app.post('/reg',function(req,res){

    data = {uname: req.body.t0, email: req.body.t1, pass: req.body.t2};
    sql = "INSERT INTO users SET ?";
    query = conn.query(sql, data,(err, results) => {
      if(err) throw err;
      res.redirect('/');
    });
});

app.post('/login',function(req,res){

    
    email= req.body.t1;
    pass= req.body.t2;
    sql = "SELECT * FROM users WHERE email=? and pass=?";
    query = conn.query(sql,[email,pass],function(err,result) {
      if(err) throw err;
      if (result[0] == null)
      {
        str = "Incorrect Email or Password";
        res.render("index",{str});
      }
      else
      {
        sess=req.session;
        sess.uname = result[0].uname;
        str = sess.uname; 
        res.render("home",{str});
        
      }
    });
});

app.get('/logout',function(req,res){
  req.session.destroy(function(err){
    if(err) throw (err);
    });
    res.redirect('/');
});
*/
app.listen(3000,function(){
    console.log("Server Running");
});